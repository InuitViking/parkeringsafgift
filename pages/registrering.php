<main>

	<h1>Velkommen til EUC Syd Sønderborg</h1>
	<p>Skriv venligst din nummerplade</p>

<?php

# Checks if $_GET['e'] exists
# 'e' stands for "error", for error code stuff
if(isset($_GET['e'])){

	# A switch which takes the value from $_GET['e'] and spits out correspondings warnings/errors
	switch ($_GET['e']) {
		case 1:
			echo "Du skal indtaste en nummerplade!";
			break;
		case 2:
			echo "Nummerpladen er ikke registreret eller er ikke på parkeringspladsen.";
			break;
		case 3:
			echo "Bilen er allerede registreret.";
			break;
	}

}

?>

	<!--This form takes the registration number and sends it with POST.-->
	<form action="" method="post">
		<label for="registrationPlate">Nummerplade</label>
		<input type="text" name="registrationPlate" maxlength="7" placeholder="f.eks. AB01001">
		<input type="submit" name="register" value="Registrer">

	</form>

	<p>Ændret nummerplade? <a href="login">Log ind her</a></p>

	<p><b>OBS!<b> Denne del ville selvfølgelig ikke være vist på den rigtige vare.<br>
	Den er der kun for at simulere at en bil er parkeret</p>
	<!--This link sends the user to the same page they're on, but with the URL parameter "car". This parameter is empty-->
	<a href="registrering&car">Parkér bil</a><br>
	<p>Din nummerplade: <?=$_GET['np']?></p>

	<?php

		# Checks if the "car" parameter exists
	if(isset($_GET['car'])){

		# Some empty variables. PHP  likes that you declare them thatn just "$var;" alone
		$letters = '';
		$numbers = '';

		# Here we're splitting the English alphabet and put it in a variable called $seed
		$seed = str_split('abcdefghijklmnopqrstuvwxyz');
		# We mix the letters
		shuffle($seed);
		# Here we run them through a foreach twice
		# and cannotate two random letters in $letters as a string
		foreach (array_rand($seed, 2) as $k) $letters .= $seed[$k];
		# Here we make a random 5 digit int
		$numbers = rand(pow(10, 5-1), pow(10, 5)-1);
		# Here we put them together; integers become a string
		$registrationPlate = $letters.$numbers;
		# We call CRUD class
		$crud = new CRUD();
		# We call DateTime class (built-in PHP)
		# This is now
		$now = new DateTime;
		# We format it so it looks like we want it to
		# In this case, it needs to work in the DB
		# so year, month, day, hours, minutes, and seconds
		$now = $now->format("Y-m-d H:i:s");
		# We use the Create method from CRUD, and register our randomly
		# generated registration number as parked
		$crud->Create('regPlates',['regPlate'=>$registrationPlate,'timeParked'=>$now]);
		# We send the user we are on, but with the np parameter
		# which contains the randomly generated registration number
		header("Location: registrering&np=$registrationPlate");
	}

?>
<?php

	# If the user has clicked on "Registrer"
	if(isset($_POST['register'])){

		# Put the registration number in a variable
		$regPlate = $_POST['registrationPlate'];
		# We call the User class
		$register = new User();
		# We use the RegPark method from User class to approve the registration number
		$register->RegPark($regPlate);

	}

	?>
</main>
