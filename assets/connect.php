<?php
# THIS FILE WILL BE REMOVED IN THE FUTURE
# Sets the timezone to Danish (Will be a class thing, and should be more dynamic)
date_default_timezone_set('Europe/Copenhagen');

# Start a session
session_start();

$host = 'localhost';
$username = 'Username';
$password = 'password';
$db = 'db';

# Make a variable which call mysqli to connect to the database or die if it fails
$con = new mysqli($host, $username, $password, $db) or die("Unable to connect");
# Set everything to be utf8
$con->set_charset("utf8");
# Make $con global
global $con;
