<?php

/*
This class has four methods.
The name comes from the first letter of the name of the functions:
Create, Read, Update, and Delete
Create creates a new row in a table
Read reads a table
Update updates a row a table
and Delete deletes a row in a table
*/
class CRUD{

	# This method creates a new row in a specified table.
	public function Create($table, array $rows){

		# Take $con
		global $con;

		# Some empty variables
		$columns = '';
		$values = '';

		# Run $rows through a foreach because we want key and value
		foreach($rows as $rowName => $rowValue){

			# key are the column names from the table
			$columns .= "$rowName,";
			# values are what is supposed to be in the specific column names (keys)
			$values .= "'$rowValue',";

		}

		# Remove trailing comma
		$columns = substr_replace($columns, "", -1);
		$values = substr_replace($values, "", -1);

		# Pu the SQL query in a variable
		$sql="INSERT INTO $table ($columns) VALUES ($values)";

		# Run the query
		$con->query($sql);
	}//end of Create method

	/*
	This method reads a table
	*/
	public function Read($table, array $rows, $other=""){
		# Take $con
		global $con;
		# Make an empty variable
		$rowsStr ='';

		# Run the $rows array in a foreach
		foreach($rows as $rowName) {
			# append the row name sin $rowStr
			$rowsStr.= $rowName .",";

		}

		# Remove trailing comma
		$rowsStr = substr_replace($rowsStr, "", -1);

		# Run a query
		$result = $con->query("SELECT $rowsStr FROM $table $other");

		# Return the result
		return $result;

	}//end of Read method

	/*
	This method updates a table
	*/
	public function Update($table, array $rowArray, $id){

		# Take $con
		global $con;
		# Empty variable
		$rowsUpdate = '';

		# Run $rowArray	through a foreach where you specify key and value
		foreach($rowArray as $tableName => $tableValue){

			# Put it in $rowsUpdate
			$rowsUpdate .= $tableName."='".$tableValue."',";

		}
		# Remove trailing comma
		$rowsUpdate = substr_replace($rowsUpdate, "", -1);

		# SQL query in variable
		$sql = "UPDATE $table SET $rowsUpdate WHERE id = $id";

		# Run the query
		$con->query($sql);

	}//end of Update method

	/*
	This method deletes a id-specified row from a specified table. That's it
	*/
	public function Delete($table,$id){
		# take $con
		global $con;
		# escape string and wonder why you didn't do that with the other methods
		$table = $con->real_escape_string($table);
		$id = $con->real_escape_string($id);

		# Run query
		$con->query("DELETE FROM $table WHERE id=$id");

	}//end of Delete method

}
