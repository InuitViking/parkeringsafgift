<?php

# This class is where all user functions are, like registering a registration regPlate or logging in.
# The class might be updated to be slightly different in future
class User{

	/*
	This method registers a registratin plate in the database,
	but the registration plate has to be registered as parked,
	defined by the timeParked column in the DB. If it's null
	it's not parked, but if it's a dateTime, it is parked,
	and one can register their car.
	*/
	public function RegPark($regPlate){
		# Take $con
		global $con;
		# Call today's date
		$today = new DateTime();
		# If $regPlate isn't empty
		if($regPlate != ''){
			# Escape input
			$regPlate = $con->real_escape_string($regPlate);
			# Make all letters small
			$regPlate = strtolower($regPlate);
			# Remove whitespaces
			$regPlate = preg_replace('/\s+/', '', $regPlate);

			# Check if the registrations number exists in the database
			$checkRegPlate = $con->query("SELECT id, userId, timeParked, confirmed FROM regPlates WHERE regPlate = '$regPlate' LIMIT 1");

			# If the registrations number exists
			if(mysqli_num_rows($checkRegPlate) == 1){
				# Run it through a while loop, and put it all in an object (This is to ease up my work)
				while ($crp = $checkRegPlate->fetch_object()) {

					# If the car is parked
					if($crp->timeParked != null){

						# If it isn't confirmed
						if($crp->confirmed == 0){
							# Put timeParked in a variable.
							$timeParked = new DateTime($crp->timeParked);
							# If the users ID isn't 0 (0 is superuser)
							if($crp->userId != 0){

								# Check if the user exists
								$checkUser = $con->query("SELECT id FROM user WHERE id = ".$crp->userId." LIMIT 1");
								# If the user exists
								if(mysqli_num_rows($checkUser) == 1){

									# Make some variables related to day, time, and minutes
									$interval = $today->diff($timeParked);
									$day = $interval->format('%d');
									$hour = $interval->format('%h');
									$minutes = $interval->format('%i');

									# Format the time so it fits to the database (needs to be a certain format)
									$today = $today->format("Y-m-d H:i:s");

									# If $day, $hour is less than 0 and $minutes less than 20
									if($day > 0 && $hour > 0 && $minutes > 20){
										# Update the registrations numer where there is a user, and do not fine
										$con->query("UPDATE regPlates SET confirmed = 1, confirmationTime = '$today', ticketType = 1 WHERE userId = $crp->userId");
									}else{
										# Update the registrations number where there is a user, and fine
										$con->query("UPDATE regPlates SET confirmed = 1, confirmationTime = '$today', ticketType = 3 WHERE userId = $crp->userId");
									}

									# Send the user to an other page, and tell them the car is registered and confirmed.
									header("Location: registreret&s=processed&id=$crp->userId");

								}else{
									header('Location: registrering&e=3');
								}

							}else{
								$interval = $today->diff($timeParked);
								$day = $interval->format('%d');
								$hour = $interval->format('%h');
								$minutes = $interval->format('%i');

								$today = $today->format("Y-m-d H:i:s");

								if($day > 0 && $hour > 0 && $minutes > 20){
									$con->query("UPDATE regPlates SET confirmed = 1, confirmationTime = '$today', ticketType = 1 WHERE regPlate = '$regPlate'");
								}else{
									$con->query("UPDATE regPlates SET confirmed = 1, confirmationTime = '$today', ticketType = 3 WHERE regPlate = '$regPlate'");
								}

								header("Location: registreret&s=processed&regPlate=$regPlate");

							}

						}else{
							header('Location: registrering&e=3');
						}

					}else{

						header('Location: registrering&e=2');

					}

				}

			}else{
				Header('Location: registrering&e=2');
			}

		}else{
			header('Location: registrering&e=1');
		}

	}//end of RegPark method


	/*
	This method logs the user in, but depending on
	the user type, they go to different pages.
	There are three types:
	user
	parking guard
	administrator
	*/
	public function Login($username, $password){

		global $con;

		# If login form wasn't empty
		if($username != '' && $password != ''){

			# Escape input
			$username = $con->real_escape_string($username);
			$password = $con->real_escape_string($password);

			# Check if user exists
			$checkUser = $con->query("SELECT id, username, password, userType FROM user WHERE username='$username' LIMIT 1");

			#If the user exists
			if(mysqli_num_rows($checkUser) == 1){

				# put it all in a while to ease up my work
				while($user = $checkUser->fetch_object()){

					# if the password is correct
					if(password_verify($password, $user->password)){
						# Put their id in a session
						$_SESSION['uId'] = $user->id;
						# All according to what user type they are, go to the page tha suits the type
						switch ($user->userType) {
							case 1:
								header('Location: administration');
								break;
							case 2:
								header('Location: guard');
								break;
							case 3:
								header('Location: user');
								break;
							default://No user type assigned
								header('Location: login&e=4');
								break;
						}

					}else{
						# Wrong password (IN FUTURE DO NOT SPECIFY IF THE PASSWORD WAS WRONG)
						header('Location: login&e=2'); # CHANGED TO "2" FOR SECURITY REASONS
					}

				}

			}else{

				//User doesn't exist
				header('Location: login&e=2');

			}

		}else{
			//username and password are empty
			header('Location: login&e=1');
		}

	}//end of Login method

}
