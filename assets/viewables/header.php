<!DOCTYPE html>

<html>

	<head>

<?php
// If the user has clicked "Log off". (This will be a class item, and probably be in index.php instead)
if(isset($_GET['logoff'])){
	// Destroy the session
	session_destroy();
}

?>

		<title><?=$_GET['page'] ?></title>

		<link rel="stylesheet" type="text/css" href="assets/css/main.css">
		<meta charset="utf-8">

	</head>
