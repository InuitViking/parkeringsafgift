<?php

# This file is where everything is centered. Without this file, the thing won't work.

# require the database (This will be updated to be in a class later)
require_once('assets/connect.php');

# require necessary classes (This will be updated to be a foreach later, so it isn't hard coded, and therefore easier to update)
require_once('assets/classes/crud.php');
require_once('assets/classes/user.php');

# EVERYTHING BELOW WILL BE A SIMPLE CLASS IN THE FUTURE

# Check if "page" exists as a URL parametre
if(isset($_GET['page'])){
	# if it exists, put it in a variable to ease use
	$page = $_GET['page'];
}

# check if $page is empty
if(empty($page)){
	# if so, redirect to "registrering" (this will be updated later to use English terms [or maybe make it translatable?]
	header('Location: registrering');

# if the file doesn't exist
}elseif(!file_exists('pages/'.$page.'.php')){
	# redirect to 404 error page
	header('Location: 404');

# if everything goes as wanted
}else{

	# require necessary start start html and dependencies
	require_once('assets/viewables/header.php');

	# include the pages that are a fitting URL parameter
	include('pages/'.$page.'.php');

	# require necessary ending html and probably some javascript
	require_once('assets/viewables/footer.php');
}
