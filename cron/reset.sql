-- Delete from the "regPlates" table where "userId" is NULL
DELETE FROM `regPlates` WHERE userId IS NULL;

-- Update the "regPlates" table, and set "timeParked" to NULL, "confirmed" to "0", "confirmationTime" to "NULL", and "ticketType" to "NULL", where "userId0" is NOT "NULL"
UPDATE `regPlates` SET `timeParked`=NULL,`confirmed`=0,`confirmationTime`=NULL,`ticketType`=NULL WHERE userId IS NOT NULL;
-- This should be run every day at midnight via a cronjob (or crontab) on a server.
-- You can do so with a command in crontab like this →  mysql --user="pa" --database="pa" --password="pa" < "/var/www/parkeringsafgift/cron/reset.sql"
